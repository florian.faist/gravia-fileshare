package at.gravia.fileshare.dto.fb;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FaceBookJsonConverter {

    public static List<FbPosts> buildAlbumPosts(Path path) {
        return Arrays.stream(path.toFile().list((dir, name) -> name.endsWith(".json"))).map(storieJson -> {
            Gson gson = new Gson();
            try {
                String content = FileUtils.readFileToString(Path.of(path.toString(), storieJson).toFile(), StandardCharsets.UTF_8);
                return gson.fromJson(content, FbPosts.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }).filter(f -> f != null).collect(Collectors.toList());
    }
}
