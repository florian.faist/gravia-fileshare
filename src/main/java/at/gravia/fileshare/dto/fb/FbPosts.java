package at.gravia.fileshare.dto.fb;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FbPosts {
    private String name;
    private List<PhotoVideo> photos;
    private List<PhotoVideo> videos;
    private PhotoVideo coverPhotoVideo;
    private Long lastModifiedTimestamp;
    private String description;

    public String getName() {
        if (name == null) {
            name = "";
        }
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public List<PhotoVideo> getPhotosOrVideos() {
        List<PhotoVideo> photosVideos = new ArrayList<>();
        if (photos != null) {
            photosVideos.addAll(photos);
        }
        if (videos != null) {
            photosVideos.addAll(videos);
        }
        return photosVideos;
    }

    public void setPhotos(List<PhotoVideo> value) {
        this.photos = value;
    }

    public PhotoVideo getCoverPhoto() {
        return coverPhotoVideo;
    }

    public void setCoverPhoto(PhotoVideo value) {
        this.coverPhotoVideo = value;
    }

    public Long getLastModifiedTimestamp() {
        return lastModifiedTimestamp;
    }

    public void setLastModifiedTimestamp(Long value) {
        this.lastModifiedTimestamp = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public void updateMediaWithProcessPath(Path zipedFolderDestination) {
        if (getPhotosOrVideos() != null) {
            getPhotosOrVideos().stream().forEach(m -> m.setProcessPath(zipedFolderDestination));
        }
    }

}
