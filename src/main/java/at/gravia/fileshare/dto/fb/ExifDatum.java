package at.gravia.fileshare.dto.fb;

public class ExifDatum {
    private String uploadIP;

    public String getUploadIP() {
        return uploadIP;
    }

    public void setUploadIP(String value) {
        this.uploadIP = value;
    }
}
