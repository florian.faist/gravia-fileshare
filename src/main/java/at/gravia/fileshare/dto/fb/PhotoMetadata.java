package at.gravia.fileshare.dto.fb;

import java.util.List;

public class PhotoMetadata {
    private List<ExifDatum> exifData;

    public List<ExifDatum> getExifData() {
        return exifData;
    }

    public void setExifData(List<ExifDatum> value) {
        this.exifData = value;
    }
}
