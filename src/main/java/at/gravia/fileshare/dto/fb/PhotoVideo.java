package at.gravia.fileshare.dto.fb;

import com.google.gson.annotations.SerializedName;

import java.nio.file.Path;

public class PhotoVideo {
    private String uri;
    @SerializedName("creation_timestamp")
    private Long creationTimestamp;
    private MediaMetadata mediaMetadata;
    private String title;
    private String description;
    private Path processPath;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Long getCreationTimestamp() {
        if (creationTimestamp == null) {
            creationTimestamp = 0L;
        }
        return creationTimestamp;
    }

    public void setCreationTimestamp(Long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public MediaMetadata getMediaMetadata() {
        return mediaMetadata;
    }

    public void setMediaMetadata(MediaMetadata mediaMetadata) {
        this.mediaMetadata = mediaMetadata;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Path getProcessPath() {
        return processPath;
    }

    public void setProcessPath(Path processPath) {
        this.processPath = Path.of(processPath.toString(), uri);
    }
}
