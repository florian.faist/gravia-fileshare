package at.gravia.fileshare.dto.insta;

import com.google.gson.annotations.SerializedName;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class InstaStories implements HasInstaPost {
    @SerializedName("ig_stories")
    private List<Media> igStories;

    public List<Media> getIgStories() {
        if (igStories == null) {
            igStories = new ArrayList<>();
        }
        return igStories;
    }

    public void setIgStories(List<Media> value) {
        this.igStories = value;
    }

    public void updateMediaWithProcessPath(Path baseProcessFolderPath) {
        if (igStories != null) {
            igStories.stream().forEach(m -> m.setProcessPath(baseProcessFolderPath));
        }
    }

    @Override
    public List<Media> getMedia() {
        return getIgStories();
    }


    @Override
    public String getTitle() {
        return "";
    }
}
