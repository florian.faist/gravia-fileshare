package at.gravia.fileshare.dto.insta;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class InstaJsonConverter {

    public static InstaStories buildStories(Path path) {
        return Arrays.stream(path.toFile().list((dir, name) -> name.startsWith("stories") && name.endsWith(".json"))).findFirst().map(storieJson -> {
            Gson gson = new Gson();
            try {
                String content = FileUtils.readFileToString(Path.of(path.toString(), storieJson).toFile(), StandardCharsets.UTF_8);
                return gson.fromJson(content, InstaStories.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }).orElse(null);
    }

    public static List<InstaPosts> buildPosts(Path path) {
        return Arrays.stream(path.toFile().list((dir, name) -> name.startsWith("post") && name.endsWith(".json"))).findFirst().map(postJson -> {
            Gson gson = new Gson();
            try {
                String content = FileUtils.readFileToString(Path.of(path.toString(), postJson).toFile(), StandardCharsets.UTF_8);
                List<InstaPosts> result = gson.fromJson(content, new TypeToken<List<InstaPosts>>() {
                }.getType());
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }).orElse(Collections.emptyList());
    }


}
