package at.gravia.fileshare.dto.insta;

public class MediaMetadata {
    private PhotoMetadata photoMetadata;

    public PhotoMetadata getPhotoMetadata() { return photoMetadata; }
    public void setPhotoMetadata(PhotoMetadata value) { this.photoMetadata = value; }
}
