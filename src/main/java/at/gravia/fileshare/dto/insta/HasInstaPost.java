package at.gravia.fileshare.dto.insta;

import java.nio.file.Path;
import java.util.List;

public interface HasInstaPost {
    void updateMediaWithProcessPath(Path baseProcessFolderPath);

    List<Media> getMedia();

    String getTitle();
}
