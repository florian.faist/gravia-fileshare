package at.gravia.fileshare.dto.insta;

import com.google.gson.annotations.SerializedName;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class InstaPosts implements HasInstaPost {
    private List<Media> media;
    private String title;
    @SerializedName("creation_timestamp")
    private Long creationTimestamp;

    public List<Media> getMedia() {
        if (media == null) {
            media = new ArrayList<>();
        }
        return media;
    }

    public void setMedia(List<Media> value) {
        this.media = value;
    }

    public String getTitle() {
        if (title == null) {
            title = "";
        }
        return title;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public Long getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Long value) {
        this.creationTimestamp = value;
    }

    public void updateMediaWithProcessPath(Path baseProcessFolderPath) {
        if (media != null) {
            media.stream().forEach(m -> m.setProcessPath(baseProcessFolderPath));
        }
    }
}
