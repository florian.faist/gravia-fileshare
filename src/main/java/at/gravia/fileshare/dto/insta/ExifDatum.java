package at.gravia.fileshare.dto.insta;

public class ExifDatum {
    private String deviceID;
    private String sourceType;

    public String getDeviceID() { return deviceID; }
    public void setDeviceID(String value) { this.deviceID = value; }

    public String getSourceType() { return sourceType; }
    public void setSourceType(String value) { this.sourceType = value; }
}
