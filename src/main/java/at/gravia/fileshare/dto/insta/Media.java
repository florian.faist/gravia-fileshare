package at.gravia.fileshare.dto.insta;

import com.google.gson.annotations.SerializedName;

import java.nio.file.Path;

public class Media {
    private String uri;
    @SerializedName("creation_timestamp")
    private Long creationTimestamp;
    private String title;
    private Path processPath;
    private MediaMetadata mediaMetadata;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Long getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Long creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public String getTitle() {
        if (title == null) {
            title = "";
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Path getProcessPath() {
        return processPath;
    }

    public void setProcessPath(Path processPath) {
        this.processPath = Path.of(processPath.toString(), uri);
    }
}
