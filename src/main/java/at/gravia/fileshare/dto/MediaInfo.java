package at.gravia.fileshare.dto;

import at.gravia.fileshare.entity.UploadProcess;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class MediaInfo {
    private String user;
    private String title;
    private String beschreibung;
    private String bucketId;
    private String fileName;
    private boolean image;
    private boolean video;
    private boolean data;
    private String contentType;
    private String originalFilename;
    private InputStream inputStream;
    private String prozessId;
    private LocalDateTime creationDate;
    private List<String> tags = new ArrayList<>();

    public MediaInfo(String user, String title, String beschreibung, String bucketId, String fileName, boolean image, boolean video, boolean data, String contentType, String originalFilename, InputStream inputStream, String prozessId, LocalDateTime creationDate) {
        this.user = user;
        this.title = title;
        this.beschreibung = beschreibung;
        this.bucketId = bucketId;
        this.fileName = fileName;
        this.image = image;
        this.video = video;
        this.data = data;
        this.contentType = contentType;
        this.originalFilename = originalFilename;
        this.inputStream = inputStream;
        this.prozessId = prozessId;
        this.creationDate = creationDate;
    }

    public MediaInfo(ZipMediaData media, UploadProcess uploadProcess) {
        setOriginalFilename(media.getPath().toString());
        setFileName(media.getPath().getFileName().toString());
        setBucketId(uploadProcess.getBucketId());
        setImage(FileType.IMAGE.equals(media.getFileType()));
        setVideo(FileType.VIDEO.equals(media.getFileType()));
        setTitle(media.getTitle());
        setBeschreibung(media.getDescription());
        setData(false);
        setProzessId(uploadProcess.getId());
        setUser(uploadProcess.getUser());
        setCreationDate(media.getCreatedDate());
        try {
            var contentType = Files.probeContentType(media.getPath());
            setContentType(contentType);
        } catch (IOException e) {
            log.error("Error elect Content Type", e);
        }
        try {
            setInputStream(new FileInputStream(media.getPath().toFile()));
        } catch (FileNotFoundException e) {
            log.error("Error during setting InptStream in Zip Proc", e);
        }
        getTags().addAll(media.getTags());
    }

    private ByteArrayOutputStream inputStreamClone = new ByteArrayOutputStream();

    public InputStream getInputStream() {
        if (inputStreamClone.size() > 0) {
            return new ByteArrayInputStream(inputStreamClone.toByteArray());
        } else {
            try {
                inputStream.transferTo(inputStreamClone);
                return new ByteArrayInputStream(inputStreamClone.toByteArray());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
