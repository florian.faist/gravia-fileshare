package at.gravia.fileshare.dto;

import java.nio.file.Path;
import java.util.Arrays;

public enum MimeType {
    JPEG("image/jpeg", "jpeg"),
    JPG("image/jpg", "jpg"),
    PNG("image/png", "png"),
    MPEG("video/mpeg", "mpeg"),
    MPG("video/mpeg", "mpg"),
    MPE("video/mpeg", "mpe"),
    MP4("video/mp4", "mp4"),
    OGG("video/ogg", "ogg"),
    OGV("video/ogg", "ogv"),
    QT("video/quicktime", "qt"),
    MOV("video/quicktime", "mov"),
    WEBM("video/webm", "webm"),
    D3GB("video/3gpp", "3gp"),
    INSTANCE("", "");

    private String mimetype;
    private String suffix;
    public static MimeType[] FOTO_MIME_TYPES = new MimeType[]{JPEG, JPG, PNG};
    public static MimeType[] VIDEO_MIME_TYPES = new MimeType[]{MPEG, MPG, MPE, MP4, OGG, OGV, QT, MOV, WEBM, D3GB};

    MimeType(String mimetype, String suffix) {
        this.mimetype = mimetype;
        this.suffix = suffix;
    }

    public String getMimetype() {
        return mimetype;
    }

    public String getSuffix() {
        return suffix;
    }

    public static MimeType findByMimeType(String mimetype) {
        return Arrays.stream(values()).filter(m -> m.getMimetype().equalsIgnoreCase(mimetype)).findFirst().orElse(null);
    }

    public static String[] getMimetypesAsStringOf(MimeType... types) {
        return Arrays.stream(types).map(type -> type.getMimetype()).toArray(String[]::new);
    }

    public static boolean isFoto(Path path) {
        return Arrays.stream(MimeType.FOTO_MIME_TYPES).anyMatch(mime -> path.toString().endsWith(mime.getSuffix()));
    }

    public static boolean isVideo(Path path) {
        return Arrays.stream(MimeType.VIDEO_MIME_TYPES).anyMatch(mime -> path.toString().endsWith(mime.getSuffix()));
    }
}
