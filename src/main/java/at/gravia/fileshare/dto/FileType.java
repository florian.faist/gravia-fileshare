package at.gravia.fileshare.dto;

public enum FileType {
    IMAGE("image"),
    THUMB("thumb"),
    VIDEO("video"),
    FILE("file");
    private String name;

    FileType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
