package at.gravia.fileshare.dto;

import at.gravia.fileshare.dto.fb.FbPosts;
import at.gravia.fileshare.dto.fb.PhotoVideo;
import at.gravia.fileshare.dto.insta.HasInstaPost;
import at.gravia.fileshare.dto.insta.Media;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ZipMediaData {
    private Path path;
    private FileType fileType;
    private String title;
    private String description;
    private LocalDateTime createdDate;
    private List<String> tags = new ArrayList<>();


    public ZipMediaData(PhotoVideo photoVideo, FbPosts fbPosts) {

        if (StringUtils.equalsAnyIgnoreCase(photoVideo.getTitle(), fbPosts.getName())) {
            if (StringUtils.isNotBlank(photoVideo.getTitle())) {
                setTitle(photoVideo.getTitle());
            } else if (StringUtils.isNotBlank(fbPosts.getName())) {
                setTitle(fbPosts.getName());
            }
        } else {
            setTitle(fbPosts.getName() + " " + photoVideo.getTitle());
        }

        if (MimeType.isVideo(photoVideo.getProcessPath())) {
            setFileType(FileType.VIDEO);
        } else if (MimeType.isFoto(photoVideo.getProcessPath())) {
            setFileType(FileType.IMAGE);
        } else {
            try {
                var contentType = Files.probeContentType(photoVideo.getProcessPath());
                if (contentType != null && contentType.contains("video")) {
                    setFileType(FileType.VIDEO);
                } else if (contentType != null && contentType.contains("image")) {
                    setFileType(FileType.IMAGE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (getFileType() == null) {
                setFileType(FileType.VIDEO);
            }
        }
        Timestamp timestamp = new Timestamp(photoVideo.getCreationTimestamp() * 1000L);
        var localDateTime2 = timestamp.toLocalDateTime();
        setCreatedDate(localDateTime2);
        setPath(photoVideo.getProcessPath());
        setDescription(photoVideo.getDescription());
        setDescription(getDescription() + " #facebook");
    }

    public ZipMediaData(HasInstaPost post, Media media) {
        if (StringUtils.equalsAnyIgnoreCase(post.getTitle(), media.getTitle())) {
            if (StringUtils.isNotBlank(post.getTitle())) {
                setTitle(post.getTitle());
            } else if (StringUtils.isNotBlank(media.getTitle())) {
                setTitle(media.getTitle());
            }
        } else {
            setTitle(media.getTitle() + " " + post.getTitle());
        }

        if (MimeType.isVideo(media.getProcessPath())) {
            setFileType(FileType.VIDEO);
        } else if (MimeType.isFoto(media.getProcessPath())) {
            setFileType(FileType.IMAGE);
        } else {
            try {
                var contentType = Files.probeContentType(media.getProcessPath());
                if (contentType != null && contentType.contains("video")) {
                    setFileType(FileType.VIDEO);
                } else if (contentType != null && contentType.contains("image")) {
                    setFileType(FileType.IMAGE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (getFileType() == null) {
                setFileType(FileType.VIDEO);
            }
        }
        Timestamp timestamp = new Timestamp(media.getCreationTimestamp() * 1000L);
        setCreatedDate(timestamp.toLocalDateTime());
        setPath(media.getProcessPath());
        setDescription(getDescription() + " #instagram");
    }

    public void setTitle(String title) {
        if (!StringUtils.contains(this.title, title)) {
            this.title = title;
        }
    }

    public String getTitle() {
        if (title == null) {
            title = "";
        }
        return title;
    }

    public String getDescription() {
        if (description == null) {
            description = "";
        }
        return description;
    }
}
