package at.gravia.fileshare.repo;

import at.gravia.fileshare.entity.UploadProcess;
import org.springframework.data.repository.CrudRepository;

public interface UploadProcessRepo extends CrudRepository<UploadProcess, String> {
}
