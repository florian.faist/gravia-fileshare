package at.gravia.fileshare.repo;

import at.gravia.fileshare.entity.MediaFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RepositoryRestResource(path = "files", collectionResourceRel = "files")
public interface FileContentRepo extends CrudRepository<MediaFile, String> {
    List<MediaFile> findAllByBucketIdAndCommonId(String bucketId, String commonId);

    List<MediaFile> findAllByBucketId(String bucketId);

    Optional<MediaFile> findByBucketIdAndCommonIdAndImageAndThumbnailFile(String bucketId, String commonId, boolean image, boolean thumbnail);

    Optional<MediaFile> findByBucketIdAndCommonIdAndThumbnailFileIsTrue(String bucketId, String commonId);

    List<MediaFile> findByBucketIdAndCommonIdAndDataIsTrue(String bucketId, String commonId);

    Optional<MediaFile> findByBucketIdAndCommonIdAndMasterFileIsTrue(String bucketId, String commonId);

    void deleteAllByBucketIdAndCommonId(String bucketId, String commonId);

    void deleteAllByBucketId(String bucketId);

    Page<MediaFile> findAllByBucketIdAndImageIsAndMasterFileIsTrue(String bucketId, boolean image, Pageable pageable);

    Page<MediaFile> findAllByBucketIdAndImageIsAndMasterFileIsTrueAndTagsIn(String bucketId, boolean image, Set<String> tags, Pageable pageable);


    Page<MediaFile> findAllByBucketIdAndVideoIsAndMasterFileIsTrue(String bucketId, boolean video, Pageable pageable);

    Page<MediaFile> findAllByBucketIdAndVideoIsAndMasterFileIsTrueAndTagsIn(String bucketId, boolean video, Set<String> tags, Pageable pageable);

    Page<MediaFile> findAllByBucketIdAndDataIsTrue(String bucketId, Pageable pageable);

}
