package at.gravia.fileshare.repo;

import at.gravia.fileshare.entity.MediaFile;
import org.springframework.content.commons.repository.ContentStore;
import org.springframework.content.rest.StoreRestResource;
import org.springframework.stereotype.Component;

@Component
@StoreRestResource
public interface FileContentStore extends ContentStore<MediaFile, String> {
}
