package at.gravia.fileshare.repo;

import at.gravia.fileshare.entity.BucketSize;
import at.gravia.fileshare.entity.MediaFile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public class FileContentRepoImpl {

    MongoTemplate mongoTemplate;

    public FileContentRepoImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    public BucketSize getBucketFileSize(String bucketId) {
        Aggregation aggregation = newAggregation(
                match(Criteria.where("bucketId").is(bucketId)),
                group("bucketId").sum("contentLength").as("total"));

        AggregationResults<BucketSize> groupResults = mongoTemplate.aggregate(
                aggregation, MediaFile.class, BucketSize.class);
        BucketSize bucketSize = groupResults.getUniqueMappedResult();

        return bucketSize;
    }
}
