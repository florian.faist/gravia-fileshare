package at.gravia.fileshare.controller;

public class Routes {
    public static final String UPLOAD_URL = "/g/file";
    public static final String DOWNLOAD_URL = "/p/file/{bucketid}/{type}/{commonid}";
    public static final String FILE_DOWNLOAD_URL = "/p/file/{bucketid}/{type}/{commonid}/{dateiName}";
    public static final String MEDIA_INFO_URL = "/g/file/info/{bucketid}/{type}/{commonid}";
    public static final String MEDIA_TAGS_URL = "/g/file/tags/{bucketid}/{type}";
    public static final String DELETE_FILE_URL = "/g/file/{bucketid}/{commonId}";
    public static final String UPDATE_FILE_URL = "/g/file/{bucketid}/{commonId}";
    public static final String LIST_MEDIA_FILES_URL = "/g/files/{bucketid}/{type}"; // has paging request params with default values
    public static final String DELETE_BUCKET = "/g/files/{bucketid}";
    public static final String GET_BUCKET_SIZE = "/g/size/{bucketid}";
}
