package at.gravia.fileshare.controller;

import at.gravia.fileshare.dto.FileType;
import at.gravia.fileshare.dto.MediaInfo;
import at.gravia.fileshare.entity.MediaFile;
import at.gravia.fileshare.service.MediaFileService;
import at.gravia.fileshare.service.ZipMediaUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Set;

import static at.gravia.fileshare.controller.Routes.*;


@RestController
public class FileContentController {
    @Autowired
    private MediaFileService mediaFileService;
    @Value("${server.api.key}")
    private String apiKey;
    @Autowired
    private ZipMediaUploadService zipMediaUploadService;

    @RequestMapping(value = UPLOAD_URL, method = RequestMethod.PUT)
    public ResponseEntity<?> setContent(
            @RequestHeader("api-key") String headerApiKey,
            @RequestParam(value = "bucketid") String bucketid,
            @RequestParam(value = "user") String user,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "beschreibung", required = false) String beschreibung,
            @RequestParam(value = "image", required = false, defaultValue = "false") boolean image,
            @RequestParam(value = "video", required = false, defaultValue = "false") boolean video,
            @RequestParam(value = "zip", required = false, defaultValue = "false") boolean zip,
            @RequestParam(value = "data", required = false, defaultValue = "false") boolean data,
            @RequestParam("file") MultipartFile file) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        try {
            if (zip) {
                return ResponseEntity.ok(saveZips(bucketid, file));
            } else if (image == video && !data) {
                return new ResponseEntity<Object>("image and video cannot have the same value ", HttpStatus.INTERNAL_SERVER_ERROR);
            } else if (data) {
                return saveData(bucketid, title, user, file);
            }
            var mediaInfo = new MediaInfo(user, title, beschreibung, bucketid, null, image, video, false, file.getContentType(), file.getOriginalFilename(), file.getInputStream(), null, null);
            var result = mediaFileService.createFile(mediaInfo);
            if (result != null) {
                return ResponseEntity.ok(result);
            }
            return new ResponseEntity<Object>(HttpStatus.CONFLICT);
        } catch (IOException e) {
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String saveZips(String bucketid, MultipartFile data) {
        return zipMediaUploadService.storeAndProzessZip(bucketid, data);
    }

    private ResponseEntity<?> saveData(String bucketid, String title, String user, MultipartFile file) {
        try {
            var mediaInfo = new MediaInfo(user, title, null, bucketid, title, false, false, true, file.getContentType(), file.getOriginalFilename(), file.getInputStream(), null, null);
            var result = mediaFileService.createFile(mediaInfo);
            if (result != null) {
                mediaFileService.enrichMediaFile(result);
                return ResponseEntity.ok(result);
            }
            return new ResponseEntity<Object>(HttpStatus.CONFLICT);
        } catch (IOException e) {
            return new ResponseEntity<Object>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = DOWNLOAD_URL, method = RequestMethod.GET)
    public ResponseEntity<?> getContent(
            @PathVariable("bucketid") String bucketid,
            @PathVariable("type") String type,
            @PathVariable("commonid") String commonid) {
        if (type.equals("image")) {
            return mediaFileService.getFile(bucketid, commonid, FileType.IMAGE).map(mf -> {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentLength(mf.getContentLength());
                headers.set("Content-Type", mf.getMimeType());
                return new ResponseEntity<Object>(mf.getFile(), headers, HttpStatus.OK);
            }).orElse(new ResponseEntity<Object>(HttpStatus.NOT_FOUND));
        } else if (type.equals("video")) {
            return mediaFileService.getFile(bucketid, commonid, FileType.VIDEO).map(mf ->
                    videoStreamResponse(mf)
            ).orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
        } else if (type.equals("thumb")) {
            return mediaFileService.getFile(bucketid, commonid, FileType.THUMB).map(mf -> {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentLength(mf.getContentLength());
                headers.set("Content-Type", mf.getMimeType());
                return new ResponseEntity<Object>(mf.getFile(), headers, HttpStatus.OK);
            }).orElse(new ResponseEntity<Object>(HttpStatus.NOT_FOUND));
        } else if (type.equals("file")) {
            return mediaFileService.getFile(bucketid, commonid, FileType.FILE).map(mf -> {
                try {
                    FileSystemResource resource = new FileSystemResource(mediaFileService.getResource(mf).getFile());
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentLength(mf.getContentLength());
                    headers.set("Content-Type", mf.getMimeType());
                    return new ResponseEntity(resource, headers, HttpStatus.OK);
                } catch (IOException e) {
                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentLength(mf.getContentLength());
                    headers.set("Content-Type", mf.getMimeType());
                    return new ResponseEntity(mf.getFile(), headers, HttpStatus.OK);
                }
            }).orElse(new ResponseEntity<Object>(HttpStatus.NOT_FOUND));
        } else {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = FILE_DOWNLOAD_URL, method = RequestMethod.GET)
    public ResponseEntity<?> getFielDownload(
            @PathVariable("bucketid") String bucketid,
            @PathVariable("type") String type,
            @PathVariable("commonid") String commonid,
            @PathVariable("dateiName") String dateiName) {
        return mediaFileService.getFile(bucketid, commonid, FileType.FILE).map(mf -> {
            try {
                FileSystemResource resource = new FileSystemResource(mediaFileService.getResource(mf).getFile());
                HttpHeaders headers = new HttpHeaders();
                headers.setContentLength(mf.getContentLength());
                headers.set("Content-Type", mf.getMimeType());
                headers.setContentDispositionFormData("inline", mf.getOrinalFileName());
                return new ResponseEntity(resource, headers, HttpStatus.OK);
            } catch (IOException e) {
                HttpHeaders headers = new HttpHeaders();
                headers.setContentLength(mf.getContentLength());
                headers.set("Content-Type", mf.getMimeType());
                return new ResponseEntity(mf.getFile(), headers, HttpStatus.OK);
            }
        }).orElse(new ResponseEntity<Object>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = MEDIA_INFO_URL, method = RequestMethod.GET)
    public ResponseEntity<?> getContentInfo(
            @RequestHeader("api-key") String headerApiKey,
            @PathVariable("bucketid") String bucketid,
            @PathVariable("type") String type,
            @PathVariable("commonid") String commonid) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        if (type.equals("image")) {
            return mediaFileService.getFileInfo(bucketid, commonid, FileType.IMAGE).map(mf ->
                    ResponseEntity.ok(mf)
            ).orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
        } else if (type.equals("video")) {
            return mediaFileService.getFileInfo(bucketid, commonid, FileType.VIDEO).map(mf ->
                    ResponseEntity.ok(mf)
            ).orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
        } else if (type.equals("thumb")) {
            return mediaFileService.getFileInfo(bucketid, commonid, FileType.THUMB).map(mf ->
                    ResponseEntity.ok(mf)
            ).orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
        } else if (type.equals("file")) {
            return mediaFileService.getFileInfo(bucketid, commonid, FileType.FILE).map(mf -> {
                        mediaFileService.enrichMediaFile(mf);
                        return ResponseEntity.ok(mf);
                    }
            ).orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = MEDIA_TAGS_URL, method = RequestMethod.GET)
    public ResponseEntity<?> getTags(
            @RequestHeader("api-key") String headerApiKey,
            @PathVariable("bucketid") String bucketid,
            @PathVariable("type") String type) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        if (type.equals("image")) {
            return ResponseEntity.ok(mediaFileService.getAllTags(bucketid, FileType.IMAGE));
        } else if (type.equals("video")) {
            return ResponseEntity.ok(mediaFileService.getAllTags(bucketid, FileType.VIDEO));
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = DELETE_FILE_URL, method = RequestMethod.DELETE)
    public ResponseEntity<?> delteContent(@RequestHeader("api-key") String headerApiKey, @PathVariable("bucketid") String bucketid, @PathVariable("commonId") String commonId) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        mediaFileService.deleteFile(bucketid, commonId);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @RequestMapping(value = UPDATE_FILE_URL, method = RequestMethod.POST)
    public ResponseEntity<?> updateFile(
            @RequestHeader("api-key") String headerApiKey,
            @PathVariable("bucketid") String bucketid,
            @PathVariable("commonId") String commonId,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "beschreibung", required = false) String beschreibung) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        return mediaFileService.updateMasterFile(bucketid, commonId, title, beschreibung)
                .map(mf -> ResponseEntity.ok(mf))
                .orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = LIST_MEDIA_FILES_URL, method = RequestMethod.GET)
    public ResponseEntity<?> pageAllFilesOfBucketAndType(
            @RequestHeader("api-key") String headerApiKey,
            @PathVariable("bucketid") String bucketid,
            @PathVariable("type") String type,
            @RequestParam(value = "tagfilter", required = false, defaultValue = "") Set<String> tagsFilter,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "pageSize", defaultValue = "20") Integer pageSize) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        if (type.equals("image") || type.equals("thumb")) {
            return ResponseEntity.ok(mediaFileService.getFilesInfo(bucketid, FileType.IMAGE, tagsFilter, PageRequest.of(page, pageSize, Sort.by("createdDate").descending())));
        } else if (type.equals("video")) {
            return ResponseEntity.ok(mediaFileService.getFilesInfo(bucketid, FileType.VIDEO, tagsFilter, PageRequest.of(page, pageSize, Sort.by("createdDate").descending())));
        } else if (type.equals("file")) {
            return ResponseEntity.ok(mediaFileService.getFilesInfo(bucketid, FileType.FILE, tagsFilter, PageRequest.of(page, pageSize, Sort.by("createdDate").descending())));
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = DELETE_BUCKET, method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFilesInBucket(@RequestHeader("api-key") String headerApiKey, @PathVariable("bucketid") String bucketid) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }
        mediaFileService.deleteFilesInBucket(bucketid);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @RequestMapping(value = GET_BUCKET_SIZE, method = RequestMethod.GET)
    public ResponseEntity<Long> getBucketSize(@RequestHeader("api-key") String headerApiKey, @PathVariable("bucketid") String bucketid) {
        if (!isApiKeyValid(headerApiKey)) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(mediaFileService.getBucketSizeInByte(bucketid));
    }

    private ResponseEntity videoStreamResponse(MediaFile mediaFile) {
        try {
            FileSystemResource resource = new FileSystemResource(mediaFileService.getResource(mediaFile).getFile());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(mediaFile.getContentLength());
            headers.set("Content-Type", mediaFile.getMimeType());

            return new ResponseEntity(resource, headers, HttpStatus.OK);
        } catch (IOException e) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentLength(mediaFile.getContentLength());
            headers.set("Content-Type", mediaFile.getMimeType());

            return new ResponseEntity(mediaFile.getFile(), headers, HttpStatus.OK);
        }

    }

    private boolean isApiKeyValid(String key) {
        return key.equals(this.apiKey);
    }
}