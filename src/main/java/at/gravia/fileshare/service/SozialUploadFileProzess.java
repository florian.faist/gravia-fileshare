package at.gravia.fileshare.service;

import at.gravia.fileshare.dto.FileType;
import at.gravia.fileshare.dto.MediaInfo;
import at.gravia.fileshare.dto.MimeType;
import at.gravia.fileshare.dto.ZipMediaData;
import at.gravia.fileshare.dto.fb.FaceBookJsonConverter;
import at.gravia.fileshare.dto.insta.HasInstaPost;
import at.gravia.fileshare.dto.insta.InstaJsonConverter;
import at.gravia.fileshare.entity.Type;
import at.gravia.fileshare.entity.UploadProcess;
import at.gravia.fileshare.repo.UploadProcessRepo;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class SozialUploadFileProzess {

    private UploadProcessRepo uploadProcessRepo;
    private MediaFileService mediaFileService;

    public SozialUploadFileProzess(UploadProcessRepo uploadProcessRepo, MediaFileService mediaFileService) {
        this.uploadProcessRepo = uploadProcessRepo;
        this.mediaFileService = mediaFileService;
    }

    @Async
    public void processFile(String fileProzessId) {
        log.info("start Zip Prozess " + fileProzessId);
        this.uploadProcessRepo.findById(fileProzessId).ifPresent(procFile -> {
            File zipFile = Path.of(procFile.getProcessFilePath()).toFile();
            if (zipFile.exists()) {
                if (Type.ZIP.equals(procFile.getContentType())) {
                    try {
                        log.info("unzip folder " + fileProzessId);
                        var zipedFolderDestination = unzipFileToFolder(zipFile, procFile.getId());
                        log.info("get MediaFiles " + fileProzessId);
                        var allListedMediaFiles = findAllMediaFilesInFolder(zipedFolderDestination);
                        if (folderIsADumpOfFacebook(allListedMediaFiles)) {
                            log.info("prepare for Facebook " + fileProzessId);
                            allListedMediaFiles = prepareMediaFilesWithFacebookContent(allListedMediaFiles, zipedFolderDestination);
                            if (hasFacebookJsonFormatedConten(zipedFolderDestination)) {
                                var zipedMediaFiles = processFacebokMediaFilesWithJsonData(zipedFolderDestination);
                                if (zipedMediaFiles != null && !zipedMediaFiles.isEmpty()) {
                                    allListedMediaFiles = new ArrayList<>(zipedMediaFiles);
                                }
                            }
                        } else if (folderIsADumpOfInstagram(allListedMediaFiles)) {
                            log.info("prepare for Instagram " + fileProzessId);
                            allListedMediaFiles = prepareMediaFilesWithInstagramContent(allListedMediaFiles, zipedFolderDestination);
                            if (hasInstaJsonFormatedConten(zipedFolderDestination)) {
                                var zipedMediaFiles = processInstaMediaFilesWithJsonData(zipedFolderDestination);
                                if (zipedMediaFiles != null && !zipedMediaFiles.isEmpty()) {
                                    allListedMediaFiles = zipedMediaFiles;
                                }
                            }
                        }
                        log.info("prozes  " + allListedMediaFiles.size() + " MediaFiles proc " + fileProzessId);
                        processAllMediaFiles(allListedMediaFiles, procFile);

                        log.info("clean up " + fileProzessId);
                        uploadProcessRepo.deleteById(fileProzessId);
                        zipFile.delete();
                        zipedFolderDestination.toFile().delete();
                        log.info("finish " + fileProzessId);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                this.uploadProcessRepo.deleteById(fileProzessId);
            }
        });
    }

    private Set<ZipMediaData> processFacebokMediaFilesWithJsonData(Path zipedFolderDestination) {
        Set<ZipMediaData> zipMediaDataFiles = new HashSet<>();
        zipMediaDataFiles.addAll(
                FaceBookJsonConverter.buildAlbumPosts(Path.of(zipedFolderDestination.toString(), "photos_and_videos", "album"))
                        .stream().flatMap(post -> {
                    post.updateMediaWithProcessPath(zipedFolderDestination);
                    return post.getPhotosOrVideos().stream().map(photo -> new ZipMediaData(photo, post));
                }).collect(Collectors.toList()));
        zipMediaDataFiles.addAll(
                FaceBookJsonConverter.buildAlbumPosts(Path.of(zipedFolderDestination.toString(), "photos_and_videos"))
                        .stream().flatMap(post -> {
                    post.updateMediaWithProcessPath(zipedFolderDestination);
                    return post.getPhotosOrVideos().stream().map(photo -> new ZipMediaData(photo, post));
                }).collect(Collectors.toList()));
        return zipMediaDataFiles;
    }

    private List<ZipMediaData> processInstaMediaFilesWithJsonData(Path zipedFolderDestination) {
        var instaPosts = InstaJsonConverter.buildPosts(Path.of(zipedFolderDestination.toString(), "content"));
        var instaStories = InstaJsonConverter.buildStories(Path.of(zipedFolderDestination.toString(), "content"));
        var zipList = new ArrayList<ZipMediaData>();
        zipList.addAll(processInstaPosts(instaPosts, zipedFolderDestination));
        zipList.addAll(processInstaPosts(Collections.singletonList(instaStories), zipedFolderDestination));
        return zipList;
    }

    private List<ZipMediaData> processInstaPosts(List<? extends HasInstaPost> instaPosts, Path zipedFolderDestination) {
        if (instaPosts != null) {
            return instaPosts.stream().flatMap(post -> {
                post.updateMediaWithProcessPath(zipedFolderDestination);
                return post.getMedia().stream().map(media -> new ZipMediaData(post, media));
            }).filter(f -> f != null)
                    .filter(f -> f.getFileType() != null)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private boolean hasInstaJsonFormatedConten(Path zipedFolderDestination) {
        return !getFilesOfType("json", Path.of(zipedFolderDestination.toString(), "content")).isEmpty();
    }

    private boolean hasFacebookJsonFormatedConten(Path zipedFolderDestination) {
        return !getFilesOfType("json", Path.of(zipedFolderDestination.toString(), "photos_and_videos", "album")).isEmpty();
    }

    private List<ZipMediaData> prepareMediaFilesWithFacebookContent(List<ZipMediaData> allListedMediaFiles, Path zipedFolderDestination) {
        //remove all MediaFiles who are not in photos_and_videos subpath
        allListedMediaFiles = allListedMediaFiles.stream().filter(data -> data.getPath().toString().contains("photos_and_videos")).collect(Collectors.toList());
        return allListedMediaFiles;
    }

    private List<ZipMediaData> prepareMediaFilesWithInstagramContent(List<ZipMediaData> allListedMediaFiles, Path zipedFolderDestination) {
        //remove all MediaFiles who are not in media subpath
        allListedMediaFiles = allListedMediaFiles.stream().filter(data -> data.getPath().toString().contains("media")).collect(Collectors.toList());
        return allListedMediaFiles;
    }

    private boolean folderIsADumpOfInstagram(List<ZipMediaData> allListedMediaFiles) {
        return allListedMediaFiles.stream().anyMatch(data -> data.getPath().toString().contains("media"));
    }

    private boolean folderIsADumpOfFacebook(List<ZipMediaData> allListedMediaFiles) {
        return allListedMediaFiles.stream().anyMatch(data -> data.getPath().toString().contains("photos_and_videos"));
    }

    private Optional<File> getFileContainingString(String arg, Path folderToSearch) {
        try (Stream<Path> walk = Files.walk(folderToSearch)) {
            return walk.filter(p -> !Files.isDirectory(p))
                    .filter(p -> p.toString().endsWith("html"))
                    .map(p -> {
                                try {
                                    String content = FileUtils.readFileToString(p.toFile(), StandardCharsets.UTF_8);
                                    if (content.contains(arg)) {
                                        return p.toFile();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return null;
                            }
                    ).filter(b -> b != null).findFirst();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private List<Path> getFilesOfType(String type, Path folderToSearch) {
        try (Stream<Path> walk = Files.walk(folderToSearch)) {
            return walk.filter(p -> !Files.isDirectory(p))
                    .filter(p -> p.toString().endsWith(type)).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private void processAllMediaFiles(List<ZipMediaData> mediaDataList, UploadProcess uploadProcess) {
        mediaDataList.forEach(media -> {
            try {
                var mediaFile = mediaFileService.createFileASync(new MediaInfo(media, uploadProcess), false);
                uploadProcess.getAllProcessedFiles().add(mediaFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private List<ZipMediaData> findAllMediaFilesInFolder(Path folderToSearch) throws IOException {
        List<ZipMediaData> result;
        try (Stream<Path> walk = Files.walk(folderToSearch)) {
            result = walk.filter(p -> !Files.isDirectory(p))
                    .filter(p ->
                            Arrays.stream(MimeType.FOTO_MIME_TYPES).anyMatch(mime -> p.toString().endsWith(mime.getSuffix())) ||
                                    Arrays.stream(MimeType.VIDEO_MIME_TYPES).anyMatch(mime -> p.toString().endsWith(mime.getSuffix())))
                    .map(p -> {
                                if (MimeType.isFoto(p)) {
                                    ZipMediaData zipMediaData = new ZipMediaData();
                                    zipMediaData.setPath(p);
                                    zipMediaData.setFileType(FileType.IMAGE);
                                    return zipMediaData;
                                } else if (MimeType.isVideo(p)) {
                                    ZipMediaData zipMediaData = new ZipMediaData();
                                    zipMediaData.setPath(p);
                                    zipMediaData.setFileType(FileType.VIDEO);
                                    return zipMediaData;
                                }
                                return null;
                            }
                    ).filter(p -> p != null)
                    .collect(Collectors.toList());        // collect all matched to a List
        }
        return result;
    }


    private Path unzipFileToFolder(File zipFile, String folderid) throws IOException, IllegalAccessException {
        Path dirPath = Path.of(System.getProperty("java.io.tmpdir") + File.separator + folderid);
        var zipDestinationPath = dirPath;
        if (!dirPath.toFile().exists()) {
            zipDestinationPath = Files.createDirectory(dirPath);
        }

        unzip(zipFile, zipDestinationPath);
        return zipDestinationPath;
    }

    private void unzip(File srcZip, Path destination) throws IllegalAccessException, ZipException {
        try {
            ZipFile zipFile = new ZipFile(srcZip);
            if (zipFile.isEncrypted()) {
                log.error("The zipfile is Encrypted");
                throw new IllegalAccessException("The Zip is Encrypted - encryption not supported");
            }
            zipFile.extractAll(destination.toString());
        } catch (ZipException e) {
            e.printStackTrace();
            log.error("Error during unzip", e);
            throw e;
        }
    }
}
