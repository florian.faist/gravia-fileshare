package at.gravia.fileshare.service;

import at.gravia.fileshare.entity.Type;
import at.gravia.fileshare.entity.UploadProcess;
import at.gravia.fileshare.repo.UploadProcessRepo;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class ZipMediaUploadService {

    private UploadProcessRepo uploadProcessRepo;
    private SozialUploadFileProzess sozialUploadFileProzess;

    public ZipMediaUploadService(UploadProcessRepo uploadProcessRepo, SozialUploadFileProzess sozialUploadFileProzess) {
        this.uploadProcessRepo = uploadProcessRepo;
        this.sozialUploadFileProzess = sozialUploadFileProzess;
    }

    /**
     * @param bucketId
     * @param file
     * @return ProcessId -> when it is finish
     */
    public String storeAndProzessZip(String bucketId, MultipartFile file) {
        try {
            var filePath = storeZipFileToTempFolder(bucketId, file);
            UploadProcess uploadProcess = new UploadProcess();
            uploadProcess.setProcessFilePath(filePath.toString());
            uploadProcess.setContentType(Type.ZIP);
            uploadProcess.setBucketId(bucketId);
            uploadProcessRepo.save(uploadProcess);
            this.sozialUploadFileProzess.processFile(uploadProcess.getId());
            return uploadProcess.getId();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Path storeZipFileToTempFolder(String bucketId, MultipartFile file) throws IOException {
        Path temp = Files.createTempFile("fbzip_" + bucketId + "_", ".zip");
        file.transferTo(temp);
        return temp;
    }
}
