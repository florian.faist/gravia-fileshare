package at.gravia.fileshare.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class MqService {
    private RabbitTemplate rabbitTemplate;

    public MqService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMediaReadyToQueue(String bucketid, String commonid, String type) {
        this.rabbitTemplate.convertAndSend("mediaQueue", bucketid + ":" + commonid + ":" + type);
    }
}
