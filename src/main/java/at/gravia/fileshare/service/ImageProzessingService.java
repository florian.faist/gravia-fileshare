package at.gravia.fileshare.service;

import at.gravia.fileshare.dto.FileType;
import at.gravia.fileshare.entity.MediaFile;
import at.gravia.fileshare.repo.FileContentRepo;
import at.gravia.fileshare.repo.FileContentStore;
import at.gravia.fileshare.util.MimeType;
import at.gravia.fileshare.util.imageprocessing.ImageUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Service
public class ImageProzessingService {

    private FileContentRepo filesRepo;
    private FileContentStore contentStore;
    private MqService mqService;

    public ImageProzessingService(FileContentRepo filesRepo, FileContentStore contentStore, MqService mqService) {
        this.filesRepo = filesRepo;
        this.contentStore = contentStore;
        this.mqService = mqService;
    }

    @Async
    public void asyncProzessImage(MediaFile mediaFile) throws IOException {
        prozessImage(mediaFile);
    }

    public void prozessImage(MediaFile mediaFile) throws IOException {
        var thumbNailMediaFile = storeImagesAsThumbnail(mediaFile);
        mediaFile = resizeOriginalImage(mediaFile);
        updateMasterMediaFile(mediaFile, thumbNailMediaFile);
        this.mqService.sendMediaReadyToQueue(mediaFile.getBucketId(), mediaFile.getCommonId(), FileType.IMAGE.getName());
    }

    private void updateMasterMediaFile(MediaFile mediaFile, MediaFile thumbnailMediaFile) {
        mediaFile.setThumbnailMimeType(thumbnailMediaFile.getMimeType());
        mediaFile.setContentThumbnailId(thumbnailMediaFile.getId());
        mediaFile.setInProzess(false);
        filesRepo.save(mediaFile);
    }

    private MediaFile resizeOriginalImage(MediaFile masterMedia) throws IOException {
        var origFileStream = this.contentStore.getContent(masterMedia);
        var tmpImageFileThumbnail = File.createTempFile("res_" + masterMedia.getContentId(), null);
        FileUtils.copyInputStreamToFile(origFileStream, tmpImageFileThumbnail);
        try (InputStream res = FileUtils.openInputStream(tmpImageFileThumbnail)) {
            var resizedOriginal = resizeImage(res, 1920, 1080);
            contentStore.setContent(masterMedia, resizedOriginal);
        }
        masterMedia.setMimeType(MimeType.JPG.getMimetype());
        tmpImageFileThumbnail.delete();
        return masterMedia;
    }

    private MediaFile storeImagesAsThumbnail(MediaFile masterMedia) throws IOException {
        var origFileStream = this.contentStore.getContent(masterMedia);
        var tmpImageFileThumbnail = File.createTempFile("thmp_" + masterMedia.getContentId(), null);
        FileUtils.copyInputStreamToFile(origFileStream, tmpImageFileThumbnail);
        MediaFile thumbNailMediaFile = new MediaFile();
        try (InputStream res = FileUtils.openInputStream(tmpImageFileThumbnail)) {
            var resizedThumbNail = resizeImage(res, 450, 450);
            contentStore.setContent(thumbNailMediaFile, resizedThumbNail);
        }
        thumbNailMediaFile.setMimeType(MimeType.JPG.getMimetype());
        thumbNailMediaFile.setBucketId(masterMedia.getBucketId());
        thumbNailMediaFile.setCommonId(masterMedia.getCommonId());
        thumbNailMediaFile.setThumbnailFile(true);
        var mediaFile = filesRepo.save(thumbNailMediaFile);
        tmpImageFileThumbnail.delete();
        return mediaFile;
    }

    private ByteArrayInputStream resizeImage(InputStream origImage, int targetWidth, int targetHeight) throws IOException {
        return ImageUtil.resizeImage(origImage, targetWidth, targetHeight);
    }
}
