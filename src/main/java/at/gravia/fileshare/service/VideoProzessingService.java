package at.gravia.fileshare.service;

import at.gravia.fileshare.dto.FileType;
import at.gravia.fileshare.entity.MediaFile;
import at.gravia.fileshare.repo.FileContentRepo;
import at.gravia.fileshare.repo.FileContentStore;
import at.gravia.fileshare.util.MimeType;
import at.gravia.fileshare.util.videoprocessing.MyEncoderException;
import at.gravia.fileshare.util.videoprocessing.VideoInfo;
import at.gravia.fileshare.util.videoprocessing.VideoProcessing;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ws.schild.jave.EncoderException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Service
@Slf4j
public class VideoProzessingService {
    private FileContentRepo filesRepo;
    private FileContentStore contentStore;
    private MqService mqService;

    public VideoProzessingService(FileContentRepo filesRepo, FileContentStore contentStore, MqService mqService) {
        this.filesRepo = filesRepo;
        this.contentStore = contentStore;
        this.mqService = mqService;
    }

    @Async
    public void asyncProzessVideo(MediaFile mediaFile) {
        prozessVideo(mediaFile);
    }

    public void prozessVideo(MediaFile mediaFile) {
        try {
            var encodedMp4File = videoEncoding(mediaFile);
            updateEncodedFileInRepository(encodedMp4File.encodedFile, mediaFile);
            var thumbnailMediaFile = createThumbnailFromVideoAndStore(encodedMp4File.encodedFile, encodedMp4File.videoInfo, mediaFile);
            updateMasterMediaFile(mediaFile, thumbnailMediaFile);
            deleteTempFile(encodedMp4File.encodedFile);
            mediaFile.setInProzess(false);
            filesRepo.save(mediaFile);
            this.mqService.sendMediaReadyToQueue(mediaFile.getBucketId(), mediaFile.getCommonId(), FileType.VIDEO.getName());
        } catch (Exception e) {
            log.error("Error during Video Processing", e);
        }
    }

    private void updateMasterMediaFile(MediaFile mediaFile, MediaFile thumbnailMediaFile) {
        mediaFile.setThumbnailMimeType(thumbnailMediaFile.getMimeType());
        mediaFile.setContentThumbnailId(thumbnailMediaFile.getId());
        filesRepo.save(mediaFile);
    }

    private MediaFile createThumbnailFromVideoAndStore(File encodedMp4File, VideoInfo videoInfo, MediaFile masterMediaFile) throws MyEncoderException, EncoderException, IOException {
        log.info("Create Thumbnail from Video " + encodedMp4File.getName());
        VideoProcessing videoProcessing = new VideoProcessing();
        File thumbnail = videoProcessing.generateThumbnailFromVideo(encodedMp4File, videoInfo);
        log.info("Store Thumbnail from Video " + encodedMp4File.getName());
        MediaFile thumbNailMediaFile = new MediaFile();
        try (InputStream res = FileUtils.openInputStream(thumbnail)) {
            contentStore.setContent(thumbNailMediaFile, res);
        }
        thumbNailMediaFile.setMimeType(MimeType.JPG.getMimetype());
        thumbNailMediaFile.setBucketId(masterMediaFile.getBucketId());
        thumbNailMediaFile.setCommonId(masterMediaFile.getCommonId());
        thumbNailMediaFile.setThumbnailFile(true);
        var mediaFile = filesRepo.save(thumbNailMediaFile);
        thumbnail.delete();
        return mediaFile;
    }

    private void deleteTempFile(File encodedMp4File) {
        log.info("Delete temp encoded Video file " + encodedMp4File.getName());
        encodedMp4File.delete();
    }

    private void updateEncodedFileInRepository(File encodedMp4File, MediaFile mediaFile) throws IOException {
        log.info("Update MediaFile Info " + mediaFile.getId());
        try (InputStream res = FileUtils.openInputStream(encodedMp4File)) {
            contentStore.setContent(mediaFile, res);
        }
        mediaFile.setMimeType(MimeType.MP4.getMimetype());
        filesRepo.save(mediaFile);
    }

    private EncodingPair videoEncoding(MediaFile mediaFile) throws IOException, EncoderException {
        VideoProcessing videoProcessing = new VideoProcessing();
        log.info("Start Video Encoding " + mediaFile.getContentId());
        var videoFileInputStream = contentStore.getContent(mediaFile);
        var tmpVideoFile = File.createTempFile(mediaFile.getContentId(), null);
        FileUtils.copyInputStreamToFile(videoFileInputStream, tmpVideoFile);
        var originalVideoInfo = videoProcessing.getInfo(tmpVideoFile);
        var videoAsMp4 = videoProcessing.encodeToMp4(tmpVideoFile);
        log.info("End Video Encoding " + mediaFile.getContentId());
        return new EncodingPair(videoAsMp4, originalVideoInfo);
    }

    class EncodingPair {
        File encodedFile;
        VideoInfo videoInfo;

        public EncodingPair(File encodedFile, VideoInfo videoInfo) {
            this.encodedFile = encodedFile;
            this.videoInfo = videoInfo;
        }
    }
}
