package at.gravia.fileshare.service;

import at.gravia.fileshare.controller.Routes;
import at.gravia.fileshare.dto.FileType;
import at.gravia.fileshare.entity.Links;
import at.gravia.fileshare.entity.MediaFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class LinkService {

    @Value("${server.url}")
    private String serverUrl;

    public MediaFile enrichLinks(MediaFile mediaFile) {
        if (mediaFile != null) {
            if (mediaFile.getLinks() == null) {
                mediaFile.setLinks(new Links());
            }
            if (mediaFile.isVideo()) {
                mediaFile.getLinks().setVideoUrl(buildUrl(mediaFile.getBucketId(), FileType.VIDEO.getName(), mediaFile.getCommonId()));
                mediaFile.getLinks().setThumbUrl(buildUrl(mediaFile.getBucketId(), FileType.THUMB.getName(), mediaFile.getCommonId()));
            }
            if (mediaFile.isImage()) {
                mediaFile.getLinks().setImageUrl(buildUrl(mediaFile.getBucketId(), FileType.IMAGE.getName(), mediaFile.getCommonId()));
                mediaFile.getLinks().setThumbUrl(buildUrl(mediaFile.getBucketId(), FileType.THUMB.getName(), mediaFile.getCommonId()));
            }

            if (mediaFile.isData()) {
                mediaFile.getLinks().setFileUrl(buildFileUrl(mediaFile.getBucketId(), FileType.FILE.getName(), mediaFile.getCommonId(), mediaFile.getOrinalFileName()));
            }

        }
        return mediaFile;
    }

    private String buildUrl(String bucketid, String type, String commonid) {
        var url = Routes.DOWNLOAD_URL;
        url = url.replace("{bucketid}", bucketid);
        url = url.replace("{type}", type);
        url = url.replace("{commonid}", commonid);
        return serverUrl + url;
    }

    private String buildFileUrl(String bucketid, String type, String commonid, String fileName) {
        var url = Routes.FILE_DOWNLOAD_URL;
        url = url.replace("{bucketid}", bucketid);
        url = url.replace("{type}", type);
        url = url.replace("{commonid}", commonid);
        url = url.replace("{dateiName}", fileName);
        return serverUrl + url;
    }
}
