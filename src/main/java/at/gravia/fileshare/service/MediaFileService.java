package at.gravia.fileshare.service;

import at.gravia.fileshare.dto.FileType;
import at.gravia.fileshare.dto.MediaInfo;
import at.gravia.fileshare.entity.MediaFile;
import at.gravia.fileshare.repo.FileContentRepo;
import at.gravia.fileshare.repo.FileContentRepoImpl;
import at.gravia.fileshare.repo.FileContentStore;
import at.gravia.fileshare.util.MyDateTime;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MediaFileService {

    private FileContentRepo filesRepo;
    private FileContentStore contentStore;
    private ImageProzessingService imageProzessingService;
    private VideoProzessingService videoProzessingService;
    private LinkService linkService;
    private FileContentRepoImpl fileContentRepo;

    public MediaFileService(FileContentRepo filesRepo,
                            FileContentStore contentStore,
                            ImageProzessingService imageProzessingService,
                            VideoProzessingService videoProzessingService,
                            LinkService linkService,
                            FileContentRepoImpl fileContentRepo) {
        this.filesRepo = filesRepo;
        this.contentStore = contentStore;
        this.imageProzessingService = imageProzessingService;
        this.videoProzessingService = videoProzessingService;
        this.linkService = linkService;
        this.fileContentRepo = fileContentRepo;
    }

    public MediaFile createFile(MediaInfo mediaInfo) throws IOException {
        return createFileASync(mediaInfo, true);
    }

    public MediaFile createFileASync(MediaInfo mediaInfo, Boolean async) throws IOException {
        MediaFile mediaFile = new MediaFile();
        mediaFile.setUploadDate(LocalDateTime.now());
        mediaFile.setMasterFile(true);
        mediaFile.setCommonId(UUID.randomUUID().toString());
        mediaFile.setBucketId(mediaInfo.getBucketId());
        if (mediaInfo.getCreationDate() != null) {
            mediaFile.setCreatedDate(mediaInfo.getCreationDate());
        } else {
            getCreationDateFromSrcOrDefault(mediaFile, mediaInfo);
        }
        mediaFile.setMimeType(mediaInfo.getContentType());
        if (StringUtils.isNotBlank(mediaInfo.getFileName())) {
            mediaFile.setOrinalFileName(mediaInfo.getFileName());
        } else {
            mediaFile.setOrinalFileName(mediaInfo.getOriginalFilename());
        }
        var res = contentStore.setContent(mediaFile, mediaInfo.getInputStream());
        mediaFile.setId(res.getContentId());
        mediaFile.setInProzess(true);
        mediaFile.setTitle(mediaInfo.getTitle());
        mediaFile.setBeschreibung(mediaInfo.getBeschreibung());
        mediaFile.setVideo(mediaInfo.isVideo());
        mediaFile.setImage(mediaInfo.isImage());
        mediaFile.setData(mediaInfo.isData());
        mediaFile.updateTags();
        mediaFile = filesRepo.save(mediaFile);
        if (mediaInfo.isVideo()) {
            if (async) {
                videoProzessingService.asyncProzessVideo(mediaFile);
            } else {
                videoProzessingService.prozessVideo(mediaFile);
            }
        } else if (mediaInfo.isImage()) {
            if (async) {
                imageProzessingService.asyncProzessImage(mediaFile);
            } else {
                imageProzessingService.prozessImage(mediaFile);
            }
        } else if (mediaFile.isData()) {
            mediaFile.setInProzess(false);
        }
        enrichMediaFile(mediaFile);
        return mediaFile;
    }


    private void getCreationDateFromSrcOrDefault(MediaFile mediaFile, MediaInfo mediaInfo) {
        if (mediaInfo.isImage()) {
            try {
                Metadata metadata = ImageMetadataReader.readMetadata(mediaInfo.getInputStream());
                for (Directory directory : metadata.getDirectories()) {
                    for (Tag tag : directory.getTags()) {
                        if (tag.getTagName() != null && tag.getTagName().equals("Date/Time Original") && StringUtils.isNotBlank(tag.getDescription())) {
                            mediaFile.setCreatedDate(MyDateTime.parse(tag.getDescription(), MyDateTime.EXIF_DATETIME_PATTERN));
                        }
                    }
                }
            } catch (Exception e) {
                mediaFile.setCreatedDate(LocalDateTime.now());
            }
        }
        if (mediaFile.getCreatedDate() == null) {
            mediaFile.setCreatedDate(LocalDateTime.now());
        }
    }

    public Optional<MediaFile> updateMasterFile(String bucketId, String commonId, String title, String beschreibung) {
        return filesRepo.findByBucketIdAndCommonIdAndMasterFileIsTrue(bucketId, commonId).map(masterFile -> {
            if (StringUtils.isNotBlank(title)) {
                masterFile.setTitle(title);
            }
            if (StringUtils.isNotBlank(beschreibung)) {
                masterFile.setBeschreibung(beschreibung);
            }
            masterFile.setLastModifiedDate(LocalDateTime.now());
            masterFile.updateTags();
            return filesRepo.save(masterFile);
        });
    }

    public void deleteFile(String bucketId, String commonId) {
        filesRepo.findAllByBucketIdAndCommonId(bucketId, commonId).forEach(mf -> {
            contentStore.unsetContent(mf);
        });
        filesRepo.deleteAllByBucketIdAndCommonId(bucketId, commonId);
    }

    public void deleteFilesInBucket(String bucketId) {
        filesRepo.findAllByBucketId(bucketId).forEach(mf -> {
            contentStore.unsetContent(mf);
        });
        filesRepo.deleteAllByBucketId(bucketId);
    }

    public Optional<MediaFile> getFile(String bucketId, String commonId, FileType fileType) {
        if (FileType.IMAGE.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndImageAndThumbnailFile(bucketId, commonId, true, false).flatMap(resFile ->
                    filesRepo.findByBucketIdAndCommonIdAndMasterFileIsTrue(bucketId, resFile.getCommonId())
                            .map(masterFile -> enrichMediaFileWithMaster(resFile, masterFile, true)
                            ));

        } else if (FileType.VIDEO.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndImageAndThumbnailFile(bucketId, commonId, false, false).flatMap(resFile ->
                    filesRepo.findByBucketIdAndCommonIdAndMasterFileIsTrue(bucketId, resFile.getCommonId())
                            .map(masterFile -> enrichMediaFileWithMaster(resFile, masterFile, true)
                            ));
        } else if (FileType.THUMB.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndThumbnailFileIsTrue(bucketId, commonId).flatMap(resFile ->
                    filesRepo.findByBucketIdAndCommonIdAndMasterFileIsTrue(bucketId, resFile.getCommonId())
                            .map(masterFile -> enrichMediaFileWithMaster(resFile, masterFile, true)
                            ));
        } else if (FileType.FILE.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndDataIsTrue(bucketId, commonId).stream().map(mf -> linkService.enrichLinks(mf)
            ).findFirst();
        }
        return Optional.empty();
    }

    public MediaFile enrichMediaFile(MediaFile mediaFile) {
        return linkService.enrichLinks(mediaFile);
    }

    public List<String> getAllTags(String bucketId, FileType fileType) {
        if (FileType.IMAGE.equals(fileType)) {
            var result = filesRepo.findAllByBucketIdAndImageIsAndMasterFileIsTrue(bucketId, true, Pageable.unpaged())
                    .stream().flatMap(file -> file.getTags().stream()).collect(Collectors.toSet()).stream().collect(Collectors.toSet())
                    .stream().collect(Collectors.toList());
            Collections.sort(result, String::compareTo);
            return result;
        } else if (FileType.VIDEO.equals(fileType)) {
            var result = filesRepo.findAllByBucketIdAndVideoIsAndMasterFileIsTrue(bucketId, true, Pageable.unpaged())
                    .stream().flatMap(file -> file.getTags().stream()).collect(Collectors.toSet()).stream().collect(Collectors.toSet())
                    .stream().collect(Collectors.toList());
            Collections.sort(result, String::compareTo);
            return result;
        }
        return Collections.emptyList();
    }

    public Optional<MediaFile> getFileInfo(String bucketId, String commonId, FileType fileType) {
        if (FileType.IMAGE.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndImageAndThumbnailFile(bucketId, commonId, true, false).flatMap(resFile ->
                    filesRepo.findByBucketIdAndCommonIdAndMasterFileIsTrue(bucketId, resFile.getCommonId())
                            .map(masterFile -> enrichMediaFileWithMaster(resFile, masterFile, false)
                            ));

        } else if (FileType.VIDEO.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndImageAndThumbnailFile(bucketId, commonId, false, false).flatMap(resFile ->
                    filesRepo.findByBucketIdAndCommonIdAndMasterFileIsTrue(bucketId, resFile.getCommonId())
                            .map(masterFile -> enrichMediaFileWithMaster(resFile, masterFile, false)
                            ));
        } else if (FileType.THUMB.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndThumbnailFileIsTrue(bucketId, commonId).flatMap(resFile ->
                    filesRepo.findByBucketIdAndCommonIdAndMasterFileIsTrue(bucketId, resFile.getCommonId())
                            .map(masterFile -> enrichMediaFileWithMaster(resFile, masterFile, false)
                            ));
        } else if (FileType.FILE.equals(fileType)) {
            return filesRepo.findByBucketIdAndCommonIdAndDataIsTrue(bucketId, commonId).stream().findFirst();
        }
        return Optional.empty();
    }

    public Page<MediaFile> getFilesInfo(String bucketId, FileType fileType, Set<String> tagsFilter, Pageable pageable) {
        Page<MediaFile> pageResult = Page.empty();
        if (FileType.IMAGE.equals(fileType) || FileType.THUMB.equals(fileType)) {
            if (tagsFilter != null && !tagsFilter.isEmpty()) {
                pageResult = filesRepo.findAllByBucketIdAndImageIsAndMasterFileIsTrueAndTagsIn(bucketId, true, tagsFilter, pageable);
            } else {
                pageResult = filesRepo.findAllByBucketIdAndImageIsAndMasterFileIsTrue(bucketId, true, pageable);
            }
            pageResult.get().forEach(mediaFile -> linkService.enrichLinks(mediaFile));
            return pageResult;
        } else if (FileType.VIDEO.equals(fileType)) {
            if (tagsFilter != null && !tagsFilter.isEmpty()) {
                pageResult = filesRepo.findAllByBucketIdAndVideoIsAndMasterFileIsTrueAndTagsIn(bucketId, true, tagsFilter, pageable);
            } else {
                pageResult = filesRepo.findAllByBucketIdAndVideoIsAndMasterFileIsTrue(bucketId, true, pageable);
            }
            pageResult.get().forEach(mediaFile -> linkService.enrichLinks(mediaFile));
            return pageResult;
        } else if (FileType.FILE.equals(fileType)) {
            var page = filesRepo.findAllByBucketIdAndDataIsTrue(bucketId, pageable);
            page.get().forEach(mediaFile -> linkService.enrichLinks(mediaFile));
            return page;
        }
        return Page.empty();
    }

    public MediaFile enrichMediaFileWithMaster(MediaFile res, MediaFile master, boolean withContent) {
        res.setBeschreibung(master.getBeschreibung());
        res.setTitle(master.getTitle());
        linkService.enrichLinks(res);
        if (withContent) {
            res.setFile(new InputStreamResource(contentStore.getContent(res)));
        }
        return res;
    }

    public Long getBucketSizeInByte(String bucketid) {
        var bucketSize = fileContentRepo.getBucketFileSize(bucketid);
        if (bucketSize == null) {
            return 0L;
        } else {
            return bucketSize.getTotal();
        }
    }

    public Resource getResource(MediaFile mediaFile) {
        return contentStore.getResource(mediaFile);
    }
}
