package at.gravia.fileshare.util.imageprocessing;

import com.google.common.io.Files;
import net.coobird.thumbnailator.Thumbnails;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class ImageUtil {

    public static ByteArrayInputStream resizeImage(InputStream origImage, int targetWidth, int targetHeight) throws IOException {
        BufferedImage originalImage = ImageIO.read(origImage);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Thumbnails.of(originalImage)
                .size(targetWidth, targetHeight)
                .outputFormat("JPEG")
                .outputQuality(1)
                .toOutputStream(outputStream);
        byte[] data = outputStream.toByteArray();
        return new ByteArrayInputStream(data);
    }

    public static File resizeImage(File origImage, int targetWidth, int targetHeight) throws IOException {
        BufferedImage originalImage = ImageIO.read(origImage);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Thumbnails.of(originalImage)
                .size(targetWidth, targetHeight)
                .outputFormat("JPEG")
                .outputQuality(1)
                .toOutputStream(outputStream);
        byte[] data = outputStream.toByteArray();
        var tmpFile = File.createTempFile("res_" + origImage.getName(), ".jpg");
        Files.write(data, tmpFile);
        return tmpFile;
    }
}
