package at.gravia.fileshare.util;

import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class HashTagExtractor {
    public static Set<String> extract(String text) {
        Set<String> tags = new HashSet<>();
        if (StringUtils.isNotBlank(text)) {
            String regexPattern = "(#\\w+)";
            Pattern p = Pattern.compile(regexPattern);
            Matcher m = p.matcher(text);
            while (m.find()) {
                String hashtag = m.group(1);
                tags.add(hashtag);
            }
        }
        return tags.stream().map(tag -> StringUtils.toRootLowerCase(tag)).filter(tag -> tag != null).collect(Collectors.toSet());
    }
}
