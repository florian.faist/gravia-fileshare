package at.gravia.fileshare.util;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MyDateTime {
    public static final DateTimeFormatter DATE_TIME_STAMP_FORMATTER = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static final DateTimeFormatter PAYPAL_GEB_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter EXIF_DATETIME_PATTERN = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");

    public static String formatToDate(LocalDate date) {
        if (date != null) {
            return date.format(DATE_FORMATTER);
        }
        return "-";
    }

    public static String formatToDateTime(LocalDateTime date) {
        if (date != null) {
            return date.format(DATE_TIME_FORMATTER);
        }
        return "-";
    }

    public static String formatToDate(LocalDateTime date) {
        if (date != null) {
            return date.format(DATE_FORMATTER);
        }
        return "-";
    }

    public static String formatToPayPalGebDate(LocalDateTime date) {
        if (date != null) {
            return date.format(PAYPAL_GEB_DATE);
        }
        return "-";
    }

    public static String formatToPayPalGebDate(LocalDate date) {
        if (date != null) {
            return date.format(PAYPAL_GEB_DATE);
        }
        return "-";
    }

    public static String formatDateTimeAsStamp(LocalDateTime date) {
        if (date != null) {
            return date.format(DATE_TIME_STAMP_FORMATTER);
        }
        return "-";
    }

    public static LocalDateTime parse(String arg, DateTimeFormatter pattern) {
        if (StringUtils.isNotBlank(arg)) {
            return LocalDateTime.parse(arg, pattern);
        }
        return null;
    }

    public static boolean isOlderThanHours(LocalDateTime dateToCheck, int hours) {
        var dateBeforeXHours = LocalDateTime.now().minusHours(hours).plusMinutes(1);
        return dateBeforeXHours.isAfter(dateToCheck);
    }

    public static boolean isOlderThanDays(LocalDateTime dateToCheck, int days) {
        var dateBeforeXDays = LocalDateTime.now().minusDays(days).plusMinutes(1);
        return dateBeforeXDays.isAfter(dateToCheck);
    }

    public static boolean isOlderThanDaysButNotThan(LocalDateTime dateToCheck, int days, int maxExclusiveDays) {
        return isOlderThanDays(dateToCheck, days) && !isOlderThanDays(dateToCheck, maxExclusiveDays);
    }

    public static boolean isAfterNow(LocalDateTime dateToCheck) {
        return dateToCheck.isAfter(LocalDateTime.now());
    }

    public static boolean isBetween(LocalDateTime datebetween, LocalDateTime start, LocalDateTime end) {
        return datebetween.isAfter(start) && datebetween.isBefore(end);
    }

}
