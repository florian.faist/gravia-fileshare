package at.gravia.fileshare.util.videoprocessing;

import lombok.Data;
import ws.schild.jave.EncoderException;
import ws.schild.jave.MultimediaObject;

@Data
public class VideoInfo {

    MultimediaObject multimediaObject;

    public VideoInfo(MultimediaObject multimediaObject) {
        this.multimediaObject = multimediaObject;
    }

    public boolean isPortrait() {
        try {
            var width = multimediaObject.getInfo().getVideo().getSize().getWidth();
            var height = multimediaObject.getInfo().getVideo().getSize().getHeight();
            if (width / height >= 1) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Integer getWidth() {
        try {
            return multimediaObject.getInfo().getVideo().getSize().getWidth();
        } catch (EncoderException e) {
            e.printStackTrace();
        }
        return 1280;
    }

    public Integer getHeight() {
        try {
            return multimediaObject.getInfo().getVideo().getSize().getHeight();
        } catch (EncoderException e) {
            e.printStackTrace();
        }
        return 720;
    }

    public boolean isLargerThan720p() {
        return (getWidth() * getHeight()) > (720 * 1280);
    }
}
