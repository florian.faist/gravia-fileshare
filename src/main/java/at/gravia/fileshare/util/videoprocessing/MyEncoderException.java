package at.gravia.fileshare.util.videoprocessing;

public class MyEncoderException extends Exception {

    private static final long serialVersionUID = 1L;

    MyEncoderException() {
        super();
    }

    MyEncoderException(String message) {
        super(message);
    }

    MyEncoderException(Throwable cause) {
        super(cause);
    }

    MyEncoderException(String message, Throwable cause) {
        super(message, cause);
    }
}