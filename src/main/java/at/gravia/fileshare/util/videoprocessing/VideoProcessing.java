package at.gravia.fileshare.util.videoprocessing;

import at.gravia.fileshare.util.MyDateTime;
import at.gravia.fileshare.util.imageprocessing.ImageUtil;
import ws.schild.jave.Encoder;
import ws.schild.jave.EncoderException;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.encode.AudioAttributes;
import ws.schild.jave.encode.EncodingAttributes;
import ws.schild.jave.encode.VideoAttributes;
import ws.schild.jave.encode.enums.X264_PROFILE;
import ws.schild.jave.info.VideoSize;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

public class VideoProcessing {
    private Long counter = 0l;

    public File createTempVideoFileFromInputStream(InputStream inputStream) throws IOException {
        File source = File.createTempFile(MyDateTime.formatDateTimeAsStamp(LocalDateTime.now()) + counter++, ".tmp");
        FileOutputStream fileOutputStream = new FileOutputStream(source);
        byte[] bytes = inputStream.readAllBytes();
        fileOutputStream.write(bytes);
        fileOutputStream.flush();
        fileOutputStream.close();
        return source;
    }

    public VideoInfo getInfo(File source) throws IOException, EncoderException {
        return new VideoInfo(new MultimediaObject(source));
    }

    public File encodeToMp4(File source) throws IOException, EncoderException {
        MultimediaObject multimediaObject = new MultimediaObject(source);
        File outTmpFile = null;

        outTmpFile = File.createTempFile("enc_" + source.getName(), ".mp4");
        Encoder encoder = new Encoder();
        encoder.encode(multimediaObject, outTmpFile, getEncodingAttributes(getVideoAttributes(getInfo(source)), getAudioAttributes()));

        return outTmpFile;
    }

    public File generateThumbnailFromVideo(File source, VideoInfo orignalVideo) throws MyEncoderException, EncoderException, IOException {
        MultimediaObject multimediaObject = new MultimediaObject(source);
        MyScreenExtractor instance = new MyScreenExtractor();
        var outTmpFile = File.createTempFile("thmp_" + source.getName(), ".jpg");
        var duration = multimediaObject.getInfo().getDuration();
        System.out.println("Video duration is " + duration);
        int secondsOfScreenShot = 1;
        if (duration != 0) {
            secondsOfScreenShot = (int) ((duration / 2) / 1000);
        }
        int width = -1;
        int height = -1;
        if (orignalVideo.isPortrait()) {
            width = multimediaObject.getInfo().getVideo().getSize().getWidth();
            height = multimediaObject.getInfo().getVideo().getSize().getHeight();
        }
        instance.renderOneImage(
                multimediaObject, width, height, secondsOfScreenShot, outTmpFile, 1, true);
        var resizedTmpFile = ImageUtil.resizeImage(outTmpFile, 450, 450);
        outTmpFile.delete();
        return resizedTmpFile;
    }

    private AudioAttributes getAudioAttributes() {
        AudioAttributes audio = new AudioAttributes();
        audio.setCodec("aac");
        // here 128kbit/s is 128000
        audio.setBitRate(128000);
        audio.setChannels(2);
        audio.setSamplingRate(44100);
        return audio;
    }

    private VideoAttributes getVideoAttributes(VideoInfo videoInfo) {
        VideoAttributes video = new VideoAttributes();
        video.setCodec("h264");
        video.setX264Profile(X264_PROFILE.MAIN);
        video.setQuality(5);
        // Here 1600  kbps video is 1600000
        video.setBitRate(1800000);
        // More the frames more quality and size, but keep it low based on devices like mobile
        video.setFrameRate(30);
        if (videoInfo.isLargerThan720p()) {
            video.setSize(VideoSize.hd720);
        } else {
            video.setSize(new VideoSize(videoInfo.getWidth(), videoInfo.getHeight()));
        }

        return video;
    }

    private EncodingAttributes getEncodingAttributes(VideoAttributes video, AudioAttributes audio) {
        EncodingAttributes attrs = new EncodingAttributes();
        attrs.setOutputFormat("mp4");
        attrs.setAudioAttributes(audio);
        attrs.setVideoAttributes(video);
        return attrs;
    }

}
