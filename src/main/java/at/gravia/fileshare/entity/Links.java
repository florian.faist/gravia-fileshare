package at.gravia.fileshare.entity;

import lombok.Data;

@Data
public class Links {
    private String imageUrl;
    private String videoUrl;
    private String thumbUrl;
    private String fileUrl;
}
