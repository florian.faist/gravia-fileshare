package at.gravia.fileshare.entity;

import lombok.Data;

@Data
public class BucketSize {
    private String bucketId;
    private Long total;
}
