package at.gravia.fileshare.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Document
public class UploadProcess {
    @Id
    protected String id;
    private String bucketId;
    private String user;
    private String processFilePath;
    private Type contentType;
    private List<MediaFile> allProcessedFiles = new ArrayList<>();
}
