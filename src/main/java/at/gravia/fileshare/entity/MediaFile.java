package at.gravia.fileshare.entity;

import at.gravia.fileshare.dto.MediaInfo;
import at.gravia.fileshare.util.HashTagExtractor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.content.commons.annotations.ContentId;
import org.springframework.content.commons.annotations.ContentLength;
import org.springframework.content.commons.annotations.MimeType;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@Document
public class MediaFile {

    @Id
    protected String id;
    @CreatedDate
    @Indexed(name = "create_date_index")
    protected LocalDateTime createdDate;
    protected LocalDateTime uploadDate;
    @LastModifiedDate
    protected LocalDateTime lastModifiedDate;
    @ContentId
    private String contentId;
    @ContentLength
    private long contentLength;
    @MimeType
    private String mimeType;
    @Indexed(name = "common_id_index")
    private String commonId;
    private String contentThumbnailId;
    private String thumbnailMimeType;
    private String title;
    private String beschreibung;
    @Indexed(name = "bucket_id_index")
    private String bucketId;
    private String user;
    private Set<String> tags = new HashSet<>();
    private boolean image;
    private boolean video;
    private boolean data;
    private boolean inProzess;
    private boolean masterFile;
    private boolean thumbnailFile;
    @Indexed
    private String orinalFileName;
    @Transient
    private InputStreamResource file;
    private String uploadProcessRefId;
    @Transient
    private Links links;

    public void from(MediaInfo mediaInfo) {
        this.title = mediaInfo.getTitle();
        this.beschreibung = mediaInfo.getBeschreibung();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public void updateTags() {
        getTags().addAll(HashTagExtractor.extract(this.title));
        getTags().addAll(HashTagExtractor.extract(this.beschreibung));
        var tags = getTags().stream().map(tag -> StringUtils.replace(tag, "#", "")).collect(Collectors.toSet());
        getTags().clear();
        getTags().addAll(tags);
    }
}