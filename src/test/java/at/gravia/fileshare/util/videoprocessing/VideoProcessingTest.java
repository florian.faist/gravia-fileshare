package at.gravia.fileshare.util.videoprocessing;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ws.schild.jave.EncoderException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class VideoProcessingTest {

    private static Map<String, String> videoTestMap = new HashMap<>();
    private boolean ingore = true; // ignore on Bitbucket

    static {
        videoTestMap.put("sampleAvi", "testvideo/sample-avi-file.avi");
        videoTestMap.put("sampleFlv", "testvideo/sample-flv-file.flv");
        videoTestMap.put("sampleMkv", "testvideo/sample-mkv-file.mkv");
        videoTestMap.put("sampleMov", "testvideo/sample-mov-file.mov");
        videoTestMap.put("sampleMpg", "testvideo/sample-mpg-file.mpg");
        videoTestMap.put("sampleMp4", "testvideo/sample-mp4.mp4");
        videoTestMap.put("sampleOgv", "testvideo/sample-ogv-file.ogv");
        videoTestMap.put("sampleWebm", "testvideo/sample-webm-file.webm");
        videoTestMap.put("sampleWmv", "testvideo/ sample-wmv-file.wmv");
    }

    @Test
    void testVideoEncodingAvi() {
        if (!ingore)
            videoTestMap.entrySet().stream().forEach(entry -> {
                try {
                    var file = getVideoFile(entry.getValue());
                    VideoProcessing videoProcessing = new VideoProcessing();
                    var result = videoProcessing.encodeToMp4(file);
                    Assertions.assertNotNull(result);
                } catch (Exception e) {
                    System.err.println("Error Encoding " + entry.getKey());
                    e.printStackTrace();
                }
            });
    }

    @Test
    void testVideoEncodingAndQuality() {
        if (!ingore)
            try {
                var file = getVideoFile("testvideo/qualititraining.mp4");
                VideoProcessing videoProcessing = new VideoProcessing();
                var result = videoProcessing.encodeToMp4(file);
                Assertions.assertNotNull(result);
            } catch (Exception e) {
                System.err.println("Error Encoding  qualititraining.mp4");
                e.printStackTrace();
            }
    }

    @Test
    void testVideoEncodingAndQuality2() {
        if (!ingore)
            try {
                var file = getVideoFile("testvideo/sample-video-q.mp4");
                VideoProcessing videoProcessing = new VideoProcessing();
                var result = videoProcessing.encodeToMp4(file);
                Assertions.assertNotNull(result);
            } catch (Exception e) {
                System.err.println("Error Encoding  qualititraining.mp4");
                e.printStackTrace();
            }
    }

    @Test
    void testVideoEncodingGraviaDemoVideo() {
        if (!ingore)
            try {
                var file = getVideoFile("testvideo/GraviaSpot02.mp4");
                VideoProcessing videoProcessing = new VideoProcessing();
                var result = videoProcessing.encodeToMp4(file);
                Assertions.assertNotNull(result);
            } catch (Exception e) {
                System.err.println("Error Encoding  qualititraining.mp4");
                e.printStackTrace();
            }
    }


    @Test
    void testVideoEncodingAndGenerateImage() throws IOException, EncoderException, MyEncoderException {
        if (!ingore) {
            VideoProcessing videoProcessing = new VideoProcessing();
            var originalVideoInfo = videoProcessing.getInfo(getVideoFile(videoTestMap.get("sampleMp4")));
            var result = videoProcessing.encodeToMp4(getVideoFile(videoTestMap.get("sampleMp4")));
            var res = videoProcessing.generateThumbnailFromVideo(result, originalVideoInfo);
            Assertions.assertNotNull(res);
        }

    }


    private File getVideoFile(String name) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(name).getFile());
        return file;
    }
}