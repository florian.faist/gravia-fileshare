package at.gravia.fileshare.service;

import at.gravia.fileshare.entity.MediaFile;
import at.gravia.fileshare.entity.Type;
import at.gravia.fileshare.entity.UploadProcess;
import at.gravia.fileshare.repo.FileContentRepo;
import at.gravia.fileshare.repo.FileContentRepoImpl;
import at.gravia.fileshare.repo.FileContentStore;
import at.gravia.fileshare.repo.UploadProcessRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.UUID;

class SozialUploadFileProzessTest {

    @Test
    void processFile() throws IOException {
        var uploadProcessRepoMocK = Mockito.mock(UploadProcessRepo.class);
        var fileContentRepo = Mockito.mock(FileContentRepo.class);
        var fileContentStore = Mockito.mock(FileContentStore.class);
        var imageProzessingService = Mockito.mock(ImageProzessingService.class);
        var videoProzessingService = Mockito.mock(VideoProzessingService.class);
        var linkService = Mockito.mock(LinkService.class);
        var fileContentRepoImpl = Mockito.mock(FileContentRepoImpl.class);
        var mediaFileService = new MediaFileService(fileContentRepo, fileContentStore, imageProzessingService, videoProzessingService, linkService, fileContentRepoImpl);
        var procFile = createUploadProcessTestEntity();
        Mockito.when(uploadProcessRepoMocK.findById(Mockito.any())).thenReturn(Optional.of(procFile));//setContent
        Mockito.when(fileContentStore.setContent(Mockito.any(), (InputStream) Mockito.any())).thenReturn(createDemoMediaFile());
        Mockito.when(fileContentRepo.save(Mockito.any())).then(i -> i.getArguments()[0]);
        SozialUploadFileProzess prozess = new SozialUploadFileProzess(uploadProcessRepoMocK, mediaFileService);
        prozess.processFile(procFile.getId());
        Assertions.assertFalse(procFile.getProcessFilePath().isEmpty());
    }


    private UploadProcess createUploadProcessTestEntity() {
        var mediaZipRes = getZipFileFromTestResoruces("mediazip/Instgram_florianfaist_20210325.zip");
        UploadProcess uploadProcess = new UploadProcess();
        uploadProcess.setId("demoTest01010");
        uploadProcess.setContentType(Type.ZIP);
        uploadProcess.setProcessFilePath(mediaZipRes.getPath());
        return uploadProcess;
    }

    private File getZipFileFromTestResoruces(String name) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(name).getFile());
        return file;
    }

    private MediaFile createDemoMediaFile() {
        MediaFile mf = new MediaFile();
        mf.setContentId(UUID.randomUUID().toString());
        return mf;
    }
}